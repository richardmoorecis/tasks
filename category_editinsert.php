<?php
include 'connection.php';
include 'common/configuration.php';
$username = $_SESSION['username'];



if (!isset($_SESSION['username'])) {

    location("login.php");
}

if (isset($_SESSION['catid'])) {
    $id = $_SESSION['catid'];

    $single = $randomvariable->select(
        $selectStar,
        "categories ",
        "",
        "  iCategoryId='$id' ",
        "",
        ""
    );
    $row = mysqli_fetch_array($single);
} else {
    $row = [];
}
if (isset($_POST["submit"])) {

    $name = $_POST["name"];
    $order = $_POST["order"];
    $status = $_POST["status"];
    $dt2 = date("Y-m-d h:i:s");

    if ($_FILES["uploadfile"]["name"] !== "") {

        $filename = $_FILES["uploadfile"]["name"];
        $tempname = $_FILES["uploadfile"]["tmp_name"];
        $file1 = explode(".", $filename);
        $ext = $file1[1];
        $newFileName = time();
        $folder = "upload/category/" . time() . "." . $ext;
        $allowed = array("jpg", "png", "jpeg");
        $sourceProperties = getimagesize($tempname);
        $imageType1 = $sourceProperties[2];

        switch ($imageType1) {


            case IMAGETYPE_PNG:
                $imageSrc1 = imagecreatefrompng($tempname);
                $tmp = imageResize($imageSrc1, $sourceProperties[0], $sourceProperties[1]);
                $tmpThumb = imageThumbResize($imageSrc1, $sourceProperties[0], $sourceProperties[1]);
                $name2 =  $newFileName . "." . $ext;
                imagepng($tmp, $categoryimgpath . $name2);
                imagepng($tmpThumb, $categorythumpath . $name2);
                break;

            case IMAGETYPE_JPEG:
                $imageSrc1 = imagecreatefromjpeg($tempname);
                $tmp = imageResize($imageSrc1, $sourceProperties[0], $sourceProperties[1]);
                $tmpThumb = imageThumbResize($imageSrc1, $sourceProperties[0], $sourceProperties[1]);
                $name2 =   $newFileName . "." .  $ext;
                imagejpeg($tmp, $categoryimgpath . $name2);
                imagejpeg($tmpThumb, $categorythumpath . $name2);
                break;


            default:
                echo "Invalid Image type.";
                exit;
                break;
        }


        if (isset($_SESSION['catid'])) {

            $id = $_SESSION['catid'];

            $updateimage = $randomvariable->update(
                "categories ",
                "vImage='$name2',vName='$name',iOrder='$order',enStatus='$status',dtModifiedDate='$dt2' ",
                " iCategoryId='$id' "
            );



            categoryunlinkfunction($row['vImage'], $categoryimgpath, $categorythumpath);
        } else {


            $result = $randomvariable->insert(
                "`categories` ",
                "( `vImage`, `vName`,`iOrder`, `enStatus` ,`dtAddedDate`,`dtModifiedDate`,`vUserName`) VALUES ",
                "('$name2', '$name', '$order', '$status','$dt2','$dt2','$username' ) "
            );
        }
        location("category.php");
    } else {

        if (isset($_SESSION['catid'])) {

            $id = $_SESSION['catid'];
            $updateimage = $randomvariable->update(
                "categories  ",
                " vName='$name',iOrder='$order',enStatus='$status',dtModifiedDate='$dt2' ",
                "  iCategoryId='$id' "
            );
        } else {

            $result = $randomvariable->insert(
                "`categories`",
                " (  `vName`,`iOrder`, `enStatus` ,`dtAddedDate`,`dtModifiedDate`) VALUES",
                "('$name', '$order', '$status','$dt2','$dt2' )"
            );
        }
    }

    location("category.php");
}
function imageResize($imageSrc, $imageWidth, $imageHeight)
{

    $newImageWidth = 1024;
    $newImageHeight = 720;

    $newImageLayer = imagecreatetruecolor($newImageWidth, $newImageHeight);
    imagecopyresampled($newImageLayer, $imageSrc, 0, 0, 0, 0, $newImageWidth, $newImageHeight, $imageWidth, $imageHeight);

    return $newImageLayer;
}

function imageThumbResize($imageSrc, $imageWidth, $imageHeight)
{
    $newImageWidth = 100;
    $newImageHeight = 100;

    $newImageLayer = imagecreatetruecolor($newImageWidth, $newImageHeight);
    imagecopyresampled($newImageLayer, $imageSrc, 0, 0, 0, 0, $newImageWidth, $newImageHeight, $imageWidth, $imageHeight);

    return $newImageLayer;
}


?>

<DOCTYPE html>
    <html>

    <head>
        <title>Category Edit/Insert</title>
        <?php include 'bootstrap.php'; ?>
        <link rel="stylesheet" href="style.css">
    </head>

    <body>
        <?php include 'navbar.php'; ?>
        <br>
        <br>
        <form id="form" enctype="multipart/form-data" method="POST">
            <div class="col-lg-4 m-auto">
                <br><br>
                <div class="card">

                    <div class="card-header bg-dark">
                        <h1 class="text-white text-center"> Edit/Insert Data </h1>
                    </div><br>

                    <p style="color:blue">*indicates mandatory fields</p>
                    <label> Image* </label>

                    <?php

                    if (isset($_SESSION['catid'])) { ?>


                        <img src="<?php echo "upload/category/thumbnail/" . $row['vImage']; ?>" class='imgsize' />
                        <input type="file" name="uploadfile" class="form-control">

                    <?php } else {


                        echo ("<input type='file' name='uploadfile' value='image' class='form-control' required>");
                    }
                    ?>
                    <label> Name* </label>
                    <input type="text" id="name" name="name" value="<?php echo isset($row['vName']) ? $row['vName'] : "" ?>" class="form-control" required> <br>

                    <label> Order* </label>
                    <input type="number" id="order" name="order" min="1" oninput="validity.valid||(value='');" value="<?php echo isset($row['iOrder']) ? $row['iOrder'] : "" ?>" class="form-control" required> <br>

                    <label> Status* </label>

                    <select name="status" id="status">
                        <?php
                        if (isset($_SESSION['catid'])) { ?>

                            <option name='Inactive' value='Inactive' <?php echo $row['enStatus'] == 'Inactive' ? ' selected="selected"' : ''; ?>>Inactive</option>
                            <option name='Active' value='Active' <?php echo $row['enStatus'] == 'Active' ? ' selected="selected"' : ''; ?>>Active</option>
                        <?php } else {

                            echo ("<option  name='' value=''></option>");
                            echo ("<option  name='Inactive' value='Inactive'>Inactive</option>");
                            echo ("<option  name='Active' value='Active'>Active</option>");
                        }
                        ?>

                    </select>

                    <br><br>

                    <button class="btn btn-success" type="submit" name="submit" value="upload"> Submit</button><br>

                </div>

            </div>
        </form>
        <script>
            $(document).ready(function() {
                $('#form').validate({
                    rules: {
                        name: {
                            required: true
                        },
                        order: {
                            required: true
                        },
                        status: {
                            required: true
                        }
                    },
                    messages: {
                        name: 'Please enter Name.',
                        order: 'Please enter order number.',
                        status: 'Please select any status mode.',


                    }

                });
            });
        </script>


    </body>

    </html>