<?php
include 'connection.php';

$username = $_SESSION['username'];
include 'common/configuration.php';

if (!isset($_SESSION['username'])) {

    location("login.php");
}
if (isset($_SESSION['id'])) {

    $id = $_SESSION['id'];


    $allproducts = $randomvariable->select(
        $selectStar,
        "products ",
        " ",
        "   iProductId='$id' ",
        " ",
        " "
    );
    $row = mysqli_fetch_array($allproducts);


    $allimages = $randomvariable->select(
        $selectStar,
        "product_images ",
        " ",
        " iProductId='$id' ",
        " ",
        " "
    );
} else {

    $row = [];
}

if (isset($_POST['submit'])) {
    $categoryname = $_POST['categoryname'];

    $productname = $_POST['productname'];
    $productcode = $_POST['productcode'];
    $price = $_POST['price'];
    $saleprice = $_POST['saleprice'];
    $quantity = $_POST['quantity'];
    $order = $_POST['order'];
    $status = $_POST['status'];
    $dt2 = date("Y-m-d h:i:s");


    if (isset($_SESSION['id'])) {

        $productQuery = $randomvariable->select(
            $selectStar,
            " products ",
            " ",
            " vProductCode='$productcode' ",
            " ",
            " "
        );

        if ($productQuery) {
            $productCodeInArray =  mysqli_fetch_assoc($productQuery);
        }

        if (($productCodeInArray && $productCodeInArray['iProductId'] !== $_SESSION['id']) || mysqli_num_rows($productQuery)  > 1) {
            echo "The given product code is already exists!!";
        } else {

            $id = $_SESSION['id'];

            $randomvariable->update(
                " products  ",
                " vName='$productname',vProductCode='$productcode',fPrice='$price',fSalePrice='$saleprice',iQuantity='$quantity',iOrder='$order',enStatus='$status',dtModifiedDate='$dt2', iCategoryId='$categoryname' ",
                " iProductId='$id' "
            );


            $result1 = $randomvariable->select(
                $selectStar,
                "product_images ",
                " ",
                " iProductId='$id'  and enIsMainImage='Yes' ",
                " ",
                " "
            );



            $rowForMainImage = mysqli_fetch_array($result1);


            if (isset($_POST['secondaryradiobtn'])) {

                changeToMainImage($conn, $rowForMainImage, $randomvariable);
            }

            if ($_FILES['imageFile']['tmp_name'][0] !== "") {

                imageUploadCommon($order, $dt2, $conn, $_SESSION['id'], $randomvariable, $productimagesinsert, $productimgpath, $productthumpath);
            }

            location("product.php");
        }
    } else {

        $result2 = $randomvariable->select(
            $selectStar,
            "products ",
            " ",
            " vProductCode='$productcode' ",
            " ",
            " "
        );

        if (mysqli_num_rows($result2) > 0) {
            echo "The given product code is already exists!!";
        } else {

            $sqlForProductInsert = $randomvariable->insert(
                "`products` ",
                "(`iCategoryId`,`vName`,`vProductCode`, `fPrice`,`fSalePrice`,`iQuantity`,`iOrder`,`enStatus` ,`dtAddedDate`,`dtModifiedDate`,`vUserName`) VALUES ",
                "('$categoryname','$productname', '$productcode','$price','$saleprice' ,'$quantity','$order','$status','$dt2','$dt2','$username' ) "
            );




            $result3 = $randomvariable->select(
                $selectStar,
                "products ",
                " ",
                "  vName = '$productname' ",
                " ",
                " "
            );

            $row = mysqli_fetch_assoc($result3);
            $productId = $row['iProductId'];


            imageUploadCommon($order, $dt2, $conn, $productId, $randomvariable, $productimagesinsert, $productimgpath, $productthumpath);

            location("product.php");
        }
    }
}


if (isset($_POST['imgdelete'])) {

    $id = $_POST['imgdelete'];



    $queryToGetName = $randomvariable->select(
        $selectStar,
        "product_images ",
        " ",
        " iProductImageId = '$id' ",
        " ",
        " "
    );

    $fetchedName = mysqli_fetch_assoc($queryToGetName);


    productunlinkfunction($fetchedName['vImage'], $productimgpath, $productthumpath);

    $sessid = $_SESSION['id'];


    $queryToGetAllSecondaryImage = $randomvariable->select(
        $selectStar,
        "product_images ",
        " ",
        " iProductId = '$sessid' AND iProductImageId != '$id' ",
        " ",
        " "
    );

    $image_array = mysqli_fetch_row($queryToGetAllSecondaryImage);
    $newMainImage = $image_array[0];
    $newSql = $randomvariable->update(
        "product_images  ",
        "enIsMainImage ='Yes' ",
        " iProductImageId = '$newMainImage'"
    );



    $randomvariable->delete("product_images", "iProductImageId='$id'");
}

function imageResize($imageSrc, $imageWidth, $imageHeight)
{
    $newImageWidth = 1024;
    $newImageHeight = 720;

    $newImageLayer = imagecreatetruecolor($newImageWidth, $newImageHeight);
    imagecopyresampled($newImageLayer, $imageSrc, 0, 0, 0, 0, $newImageWidth, $newImageHeight, $imageWidth, $imageHeight);

    return $newImageLayer;
}

function imageThumbResize($imageSrc, $imageWidth, $imageHeight)
{
    $newImageWidth = 100;
    $newImageHeight = 100;

    $newImageLayer = imagecreatetruecolor($newImageWidth, $newImageHeight);
    imagecopyresampled($newImageLayer, $imageSrc, 0, 0, 0, 0, $newImageWidth, $newImageHeight, $imageWidth, $imageHeight);

    return $newImageLayer;
}
function getName($n)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $randomString = '';

    for ($i = 0; $i < $n; $i++) {
        $index = rand(0, strlen($characters) - 1);
        $randomString .= $characters[$index];
    }

    return $randomString;
}

function imageUploadCommon($order, $dt2, $conn, $productId, $randomvariable, $productimagesinsert, $productimgpath, $productthumpath)
{

    foreach ($_FILES['imageFile']['tmp_name'] as $key => $image) {
        $imageTmp_Name = $_FILES['imageFile']['tmp_name'][$key];
        $imageName = $_FILES['imageFile']['name'][$key];
        $file1 = explode(".", $imageName);
        $ext = $file1[1];
        $sourceProperties1 = getimagesize($imageTmp_Name);
        $allowed = array("jpg", "png", "jpeg");
        $imageType1 = $sourceProperties1[2];
        $newFileName = time();
        switch ($imageType1) {


            case IMAGETYPE_PNG:
                $imageSrc1 = imagecreatefrompng($imageTmp_Name);
                $randomstr = getName(4);
                $tmp = imageResize($imageSrc1, $sourceProperties1[0], $sourceProperties1[1]);
                $tmpThumb = imageThumbResize($imageSrc1, $sourceProperties1[0], $sourceProperties1[1]);
                $name2 =  $newFileName . $randomstr . "." . $ext;
                imagepng($tmp, $productimgpath . $name2);
                imagepng($tmpThumb, $productthumpath . $name2);
                break;

            case IMAGETYPE_JPEG:
                $imageSrc1 = imagecreatefromjpeg($imageTmp_Name);
                $randomstr = getName(4);
                $tmp = imageResize($imageSrc1, $sourceProperties1[0], $sourceProperties1[1]);
                $tmpThumb = imageThumbResize($imageSrc1, $sourceProperties1[0], $sourceProperties1[1]);
                $name2 =  $newFileName . $randomstr . "." . $ext;
                imagejpeg($tmp, $productimgpath . $name2);
                imagejpeg($tmpThumb, $productthumpath . $name2);
                break;


            default:
                echo "Invalid Image type.";
                exit;
                break;
        }

        if ($key == 0 && !$_SESSION['id']) {

            $query = $productimagesinsert . "( 
                '$name2',
                '$order',
                '$productId',
                 'Yes',
                 '$dt2',
                 '$dt2')";
            $run = alldata($conn, $query);
        } else {
            $query = $productimagesinsert . "( '$name2','$order','$productId','NO',
            '$dt2',
            '$dt2')";
            $run = alldata($conn, $query);
        }
    }
}
function changeToMainImage($conn, $rowForMainImage, $randomvariable)
{
    $storeimage = $_POST['secondaryradiobtn'];

    $sqlquery = "UPDATE  product_images SET enIsMainImage='Yes' WHERE iProductImageId='$storeimage' ";


    $secquery = "UPDATE product_images SET enIsMainImage='No' WHERE  iProductImageId='$rowForMainImage[iProductImageId]' ";

    alldata($conn, $secquery);
    alldata($conn, $sqlquery);
}

?>
<html>

<head>
    <title>Product Edit/Insert</title>
    <?php include 'bootstrap.php'; ?>
    <link rel="stylesheet" href="style.css">

</head>

<body>
    <?php include 'navbar.php'; ?>
    <form id="form" enctype="multipart/form-data" method="POST">
        <div class="col-lg-5 m-auto">
            <br><br>
            <div class="card">
                <div class="card-header bg-dark">
                    <h1 class="text-white text-center"> INSERT/EDIT </h1>
                </div><br>
                <?php
                if (isset($_SESSION['id'])) {
                    echo ("<div class='row'>");


                    while ($rowForSecondary = mysqli_fetch_assoc($allimages)) {
                ?>

                        <div class="column show">

                            <td>
                                <p><img src="<?php echo "upload/product/thumbnail/" . $rowForSecondary['vImage']; ?>" style="width:100px" style="height:100px"></p>
                            </td>
                            <div class="styleclass">
                                <p><input type="radio" id="<?php echo $rowForSecondary['iProductImageId']; ?>" name="secondaryradiobtn" value="<?php echo $rowForSecondary['iProductImageId']; ?>" <?php echo $rowForSecondary['enIsMainImage'] == 'Yes' ? 'checked=checked' : '';  ?>> main image</p>
                                <span class="action" style="padding-left:12px"><a href="#" id="<?php echo $rowForSecondary['iProductImageId']; ?>" class="delete" title="Delete">X</a></span>
                            </div>
                        </div>
                <?php
                    }


                    echo ("</div>");
                } else {
                    echo ("<p style='color:blue'>*indicates mandatory fields </p>");

                    echo ("<label>  Insert Images* </label>");

                    echo ("<input type='file' name='imageFile[]'  multiple class='form-control' required><br>");
                }
                ?>

                <?php

                if (isset($_SESSION['id'])) {
                    echo ("<label>  Insert Images(optional) </label>");
                    echo ("<input type='file' name='imageFile[]'multiple class='form-control'><br>");
                }

                ?>

                <label> Category Name* </label>
                <select name="categoryname" id="categoryname">

                    <?php

                    $resultcat = $randomvariable->select(
                        $selectStar,
                        "categories ",
                        " ",
                        " categories.vUserName='$username' ",
                        " ",
                        " "
                    );


                    if (isset($_SESSION['id'])) {
                        while ($rowForCategory = mysqli_fetch_assoc($resultcat)) { ?>

                            <option name="<?php echo $rowForCategory['iCategoryId'] ?>" value="<?php echo $rowForCategory['iCategoryId'] ?>" <?php echo $row['iCategoryId'] == $rowForCategory['iCategoryId'] ? ' selected="selected"' : ''; ?>><?php echo $rowForCategory['vName'] ?></option>

                    <?php }
                    } else {
                        echo ("<option name='' value=''></option>");
                        while ($rowForCategory = mysqli_fetch_assoc($resultcat)) {

                            echo ("<option name='$rowForCategory[iCategoryId]' value='$rowForCategory[iCategoryId]'>$rowForCategory[vName]</option>");
                        }
                    }

                    ?>
                </select>
                <br />

                <label> Product Name* </label>
                <input type="text" name="productname" id="productname" maxlength="14" class="form-control" value="<?php echo isset($row['vName']) ? $row['vName'] : "" ?>" required> <br>


                <label> Product Code* </label>
                <input type="text" name="productcode" id="productcode" maxlength="9" class="form-control" value="<?php echo isset($row['vProductCode']) ? $row['vProductCode'] : "" ?>" required> <br>

                <label> Price* </label>
                <input type="number" name="price" id="price" class="form-control" min="0" oninput="validity.valid||(value='');" value="<?php echo isset($row['fPrice']) ? $row['fPrice'] : "" ?>" required> <br>

                <label> Sale Price </label>
                <input type="number" name="saleprice" id="saleprice" class="form-control" min="0" oninput="validity.valid||(value='');" value="<?php echo isset($row['fSalePrice']) ? $row['fSalePrice'] : "" ?>"> <br>

                <label> Quantity* </label>
                <input type="number" name="quantity" id="quantity" class="form-control" min="0" oninput="validity.valid||(value='');" value="<?php echo isset($row['iQuantity']) ? $row['iQuantity'] : "" ?>" required> <br><br />

                <label> Order* </label>
                <input type="number" name="order" id="order" class="form-control" min="0" oninput="validity.valid||(value='');" value="<?php echo isset($row['iOrder']) ? $row['iOrder'] : "" ?>" required> <br><br />

                <label> Status* </label>

                <select name="status" id="status">

                    <?php
                    if (isset($_SESSION['id'])) { ?>
                        <option name='Inactive' value='Inactive' <?php echo $row['enStatus'] == 'Inactive' ? ' selected="selected"' : ''; ?>>Inactive</option>
                        <option name='Active' value='Active' <?php echo $row['enStatus'] == 'Active' ? ' selected="selected"' : ''; ?>>Active</option>
                    <?php } else {
                        echo ("<option name='' value=''></option>");
                        echo ("<option name='Inactive' value='Inactive'>Inactive</option>");
                        echo ("<option name='Active' value='Active'>Active</option>");
                    }

                    ?>

                </select>
                <br><br>

                <button class="btn btn-success" type="submit" name="submit" value="upload"> Submit</button><br>

            </div>

        </div>
    </form>

</body>

<script>
    $(function() {
        $(".delete").click(function() {
            var element = $(this);
            var del_id = element.attr("id");
            var info = 'imgdelete=' + del_id;
            if (confirm("Delete the image")) {
                $.ajax({
                    type: "POST",
                    url: "product_editinsert.php",
                    data: info,
                    success: function() {

                    }
                });
                $(this).closest('.show').remove();
            }
            return false;
        });
    });
</script>
<script>
    $(document).ready(function() {
        $('#form').validate({
            rules: {
                categoryname: {
                    required: true,
                },
                productname: {
                    required: true,
                },

                productcode: {
                    required: true,

                },

                price: {
                    required: true,

                },
                quantity: {
                    required: true,

                },
                order: {
                    required: true,

                },
                status: {
                    required: true,

                },



            },
            messages: {
                categoryname: ' Select Any  Category.',
                productname: 'Please Enter  Product Name.',
                productcode: 'Please Enter Product Code.',
                price: 'Please Enter Price.',
                quantity: 'Please Enter Quantity.',
                order: 'Please Enter Order.',
                status: 'Select Any Status.',

            }

        });
    });
</script>

</html>