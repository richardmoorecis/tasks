<?php
include 'connection.php';
include 'common/configuration.php';

$username = "";
$email    = "";
$errors = array();

if (isset($_SESSION['username'])) {
	location("category.php");
} else {

	if (isset($_POST['reg_user'])) {
		$username = $_POST['username'];
		$email =  $_POST['email'];
		$password_1 = $_POST['password_1'];
		$password_2 = $_POST['password_2'];
		$dt1 = date("Y-m-d h:i:s");

		if (empty($username)) {
			array_push($errors, "Username is required");
		}

		if (empty($email)) {
			array_push($errors, "Email is required");
		}

		if (empty($password_1)) {
			array_push($errors, "Password is required");
		}

		if ($password_1 != $password_2) {
			array_push($errors, "Passwords should match");
		}
		$result = $randomvariable->select(
			$selectStar,
			"users ",
			"",
			"  vUserName ='$username' or vEmail='$email'",
			"",
			""
		);

		$duplicate_user = mysqli_fetch_assoc($result);


		if ($duplicate_user) {
			array_push($errors, "user already exist");
		}

		if (count($errors) == 0) {
			$password = md5($password_1);
			$randomvariable->insert(
				" `users` ",
				"(`vUserName`, `vEmail`, `vPassword`, `dtCreatedAt`, `dtLastloginAt`) VALUES ",
				"('$username', '$email', '$password','$dt1','$dt1')"
			);


			$_SESSION['username'] = $username;
			location("category.php");
		}
	}
}


?>
<html>

<head>
	<title>User-Registration</title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<div id="login">

		<div>
			<div id="login-row" class="row justify-content-center align-items-center">
				<div id="login-column" class="col-md-6">
					<div id="login-box" class="col-md-12">
						<form id="form" class="form" method="POST">
							<br />
							<br />
							<?php include('errors.php'); ?>
							<h3 class="text-center text-info">Registration Form</h3>
							<div class="form-group">
								<label for="username" class="text-info">Username:</label><br>
								<input type="text" id="username" name="username" class="form-control" value="<?php echo $username; ?>">
							</div>
							<div class="form-group">
								<label for="email" class="text-info">Email:</label><br>
								<input type="email" id="email" name="email" class="form-control" value="<?php echo $email; ?>">
							</div>

							<div class="form-group">
								<label for="password" class="text-info">Password:</label><br>
								<input type="password" id="password_1" name="password_1" class="form-control">
							</div>

							<div class="form-group">
								<label for="password" class="text-info"> Confirm Password:</label><br>
								<input type="password" id="password_2" name="password_2" class="form-control">
							</div>

							<div class="form-group text-center">

								<input type="submit" name="reg_user" id="btn-submit" class="btn btn-primary btn-md"><br /><br />
							</div>
							<p>
								<a href="login.php">Already Registered! Login</a>
							</p>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(document).ready(function() {
			$('#form').validate({
				rules: {
					username: {
						required: true
					},
					email: {
						required: true,
						email: true
					},

					password_1: {
						required: true,
						minlength: 8
					},
					password_2: {
						required: true,
						equalTo: "#password_1"
					}
				},
				messages: {
					username: 'Please enter Username.',
					email: {
						required: 'Please enter Email Address.',
						email: 'Please enter a valid Email Address.',
					},

					password_1: {
						required: 'Please enter Password.',
						minlength: 'Password must be at least 8 characters long.',
					},
					password_2: {
						required: 'Please enter Confirm Password.',
						equalTo: 'Confirm Password do not match with Password.',
					}
				},

			});
			$('#btn-submit').click(function() {
				var hasError = false;
				var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
				var emailaddressVal = $("#email").val();
				if (!emailReg.test(emailaddressVal)) {
					$("#email").after('<span class="error">Enter a valid email address.</span>');
					hasError = true;
				}

				if (hasError == true) {
					return false;
				}
			});


		});
	</script>
</body>


</html>