<?php
include 'connection.php';
include 'common/configuration.php';



$username = $_SESSION['username'];
if (!isset($_SESSION['username'])) {
    location("login.php");
}

if (isset($_SESSION['catid'])) {
    unset($_SESSION['catid']);
}

if (isset($_GET['order'])) {
    $order = $_GET['order'];
} else {
    $order = 'vName';
}
if (isset($_GET['sort'])) {
    $sort = $_GET['sort'];
} else {
    $sort = 'ASC';
}

$search = "";
$status_search = "";

if (isset($_POST['filterbutton'])) {

    if ($_POST['search'] == "" && $_POST['status_search'] == "") {

        $result = $randomvariable->select(
            $selectCategoryGetData,
            "categories ",
            $leftjoinCategoryGetData,
            "  categories.vUserName='$username' ",
            $categorygroupby,
            "ORDER BY $order $sort "
        );
    } else if ($_POST['search'] && $_POST['status_search']) {
        $search = $_POST['search'];
        $status_search = $_POST['status_search'];

        $result = $randomvariable->select(
            $selectCategoryGetData,
            "categories ",
            $leftjoinCategoryGetData,
            " categories.vUserName='$username' AND (categories.vName LIKE '%" . $search . "%') AND (categories.enStatus = '$status_search') ",
            $categorygroupby,
            "ORDER BY $order $sort "
        );
    } elseif ($_POST['search']) {
        $search = $_POST['search'];

        $result = $randomvariable->select(
            $selectCategoryGetData,
            "categories ",
            $leftjoinCategoryGetData,
            " categories.vUserName='$username' AND (categories.vName LIKE '%" . $search . "%') ",
            $categorygroupby,
            "ORDER BY $order $sort "
        );
    } else {
        $status_search = $_POST['status_search'];

        $result = $randomvariable->select(
            $selectCategoryGetData,
            "categories ",
            $leftjoinCategoryGetData,
            " categories.vUserName='$username' AND (categories.enStatus = '$status_search') ",
            $categorygroupby,
            "ORDER BY $order $sort "
        );
    }
} else {

    $result = $randomvariable->select(
        $selectCategoryGetData,
        " categories ",
        $leftjoinCategoryGetData,
        "  categories.vUserName='$username' ",
        $categorygroupby,
        " ORDER BY $order $sort "
    );
}

if ($sort == 'ASC') {
    $sort = 'DESC';
} else {
    $sort = 'ASC';
}

if (isset($_POST['editbutton'])) {
    $editval = $_POST['editbutton'];
    $_SESSION['catid'] = $editval;
    location("category_editinsert.php");
}


?>
<!DOCTYPE html>
<html>

<head>
    <title>Home</title>
    <?php include 'bootstrap.php'; ?>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <?php include 'navbar.php'; ?>
    <br>
    <br>
    <div class="container">
        <div class="col-lg-12">

            <h1 class="text-warning text-center"> Categories </h1>

            <div align="right">
                <button class="btn btn-outline-warning" type="insert"><a href="category_editinsert.php">Add Data</a></button>

            </div>

            <table id="table_data" class=" table table-striped table-bordered">

                <form method="post">


                    <input type="text" class="form-control" maxlength="14" name="search" value="<?php echo "$search" ?>">
                    <label>Status</label>
                    <select name="status_search">
                        <option></option>
                        <option value="Inactive" name="Inactive" <?php echo $status_search == 'Inactive' ? ' selected="selected"' : ''; ?>>Inactive</option>
                        <option value="Active" name="Active" <?php echo $status_search == 'Active' ? ' selected="selected"' : ''; ?>>Active</option>

                    </select>
                    <br />
                    <button type="submit" class="btn btn-primary btn-md" name="filterbutton">Filter</button>

                </form>


                <thead>


                    <tr class="bg-dark text-white text-center">
                        <?php
                        echo "<th><a href='?order=vImage&&sort=$sort'>Image</a></th>";
                        echo "<th><a href='?order=vName&&sort=$sort'>Name</a></th>";
                        echo "<th><a href='?order=iOrder&&sort=$sort'>Order</a></th>";
                        echo "<th><a href='?'>Product Count</a></th>";
                        echo "<th><a href='?order=enStatus&&sort=$sort'>Status</a></th>";
                        echo "<th><a href='?order=dtAddedDate&&sort=$sort'>Added Date</a></th>";
                        echo "<th><a href='?order=dtModifiedDate&&sort=$sort'>Modified Date</a></th>";
                        ?>
                        <th> Action </th>

                    </tr>
                </thead>

                <?php


                if (mysqli_num_rows($result) == 0) {
                    echo ("<caption class='caption'>No Records Found</caption>");
                } else {

                    while ($row = mysqli_fetch_assoc($result)) {


                        echo ("<td><img src= \"upload/category/thumbnail/" . $row['vImage']  . "\" class='imgsize' </td>");
                        echo ("<td>$row[vName]</td>");
                        echo ("<td>$row[iOrder]</td>");
                        echo ("<td>$row[productCount]</td>");
                        echo ("<td>$row[enStatus]</td>");
                        echo ("<td>$row[dtAddedDate]</td>");
                        echo ("<td>$row[dtModifiedDate]</td>");

                        echo ("<td><form method='POST'><button type='submit' name='editbutton' value='$row[iCategoryId]' class='btn btn-outline-danger' >Edit</button></form>");
                        echo (" <button type='button' class='btn btn-outline-success'><a href=\"category_delete.php?id=" . $row['iCategoryId'] . "\">  Delete </a></button></td></tr>");
                    }
                }

                ?>

            </table>
        </div>
    </div>
</body>

</html>