<?php
include 'connection.php';
$username = $_SESSION['username'];
include 'common/configuration.php';



if (!isset($_SESSION['username'])) {
    location("login.php");
}
if (isset($_SESSION['id'])) {
    unset($_SESSION['id']);
}


if (isset($_GET['order'])) {
    $order = $_GET['order'];
} else {
    $order = 'vName';
}
if (isset($_GET['sort'])) {
    $sort = $_GET['sort'];
} else {
    $sort = 'ASC';
}

$search = "";
$status_search = "";
$low = 1;
$high = 10000;
$lowquantity = 1;
$highquantity = 3000;




if (isset($_POST['filterbutton'])) {

    if ($_POST['low']) {
        $low = $_POST['low'];
    }
    if ($_POST['high']) {
        $high = $_POST['high'];
    }
    if ($_POST['lowquantity']) {
        $lowquantity = $_POST['lowquantity'];
    }
    if ($_POST['highquantity']) {
        $highquantity = $_POST['highquantity'];
    }
    if (isset($_POST['status_search'])) {
        $status_search = $_POST['status_search'];
    }
    if (isset($_POST['search'])) {
        $search = $_POST['search'];
    }


    if ($search == "" && $status_search == "" && $low == 1 && $high == 10000 && $lowquantity == 1 && $highquantity == 3000) {

        $result = $randomvariable->select(
            $selectProductGetData,
            " products ",
            $leftjoinProductGetData,
            "  products.vUserName='$username' ",
            " GROUP BY products.vName ",
            " ORDER BY $order $sort "
        );
    } else if ($status_search == "Active" || $status_search == "Inactive") {
        $result = $randomvariable->select(
            $selectProductGetData,
            " products ",
            $leftjoinProductGetData,
            "  (products.vName LIKE '%" . $search . "%')  
         AND products.vUserName='$username'
         AND (products.enStatus='$status_search') ",
            " GROUP BY products.vName ",
            " ORDER BY $order $sort "
        );
    } else {

        $result = $randomvariable->select(
            $selectProductGetData,
            " products ",
            $leftjoinProductGetData,
            " (products.vName LIKE '%" . $search . "%')  
          AND (products.fPrice >= '$low' AND products.fPrice <= '$high')  
          AND (products.iQuantity >= '$lowquantity' AND products.iQuantity <= '$highquantity') 
          AND products.vUserName='$username' ",
            " GROUP BY products.vName ",
            " ORDER BY $order $sort "
        );
    }
} else {
    $result = $randomvariable->select(
        $selectProductGetData,
        " products ",
        $leftjoinProductGetData,
        "  products.vUserName='$username'",
        " GROUP BY products.vName ",
        " ORDER BY $order $sort"
    );
}
if ($sort == 'ASC') {
    $sort = 'DESC';
} else {
    $sort = 'ASC';
}

if (isset($_POST['editbutton'])) {
    $editval = $_POST['editbutton'];
    $_SESSION['id'] = $editval;
    location("product_editinsert.php");
}



?>
<!DOCTYPE html>
<html>

<head>
    <title>Products </title>

    <?php include 'bootstrap.php'; ?>
    <link rel="stylesheet" href="style.css">
</head>

<body>
    <?php include 'navbar.php'; ?>
    <br>
    <br>
    <div class="container">
        <div class="col-lg-12">
            <br>
            <h1 class="text-primary text-center"> Products </h1>
            <p align="right">
                <button class="btn btn-outline-warning" type="insert"><a href="product_editinsert.php">Add Data</a></button>
            </p>
            <table id="table_data" class=" table table-striped table-bordered">

                <form method="post">
                    <div class="col-sm-4">

                        <label>Name</label>
                        <input type="text" class="form-control" maxlength="10" name="search" value="<?php echo "$search" ?>">
                        <label>Status</label>
                        <select name="status_search">
                            <option></option>
                            <option value="Inactive" name="Inactive" <?php echo $status_search == 'Inactive' ? ' selected="selected"' : ''; ?>>Inactive</option>
                            <option value="Active" name="Active" <?php echo $status_search == 'Active' ? ' selected="selected"' : ''; ?>>Active</option>

                        </select><br />

                        <label>Price Filter(max value:10000)</label>
                        <div class="container">

                            <div id="right" style="margin-right:20px;"><input type="number" min="0" class="form-control margin" name="low" placeholder="low value" value="<?php if (isset($_POST['low'])) echo $_POST['low'] ?>"></div>

                            <div>to</div>
                            <div id="left" style="margin-left:10px;"><input type="number" min="0" class="form-control margin" name="high" placeholder="high value" value="<?php if (isset($_POST['high'])) echo $_POST['high'] ?>"></div>
                        </div>

                        <label>Quantity Filter(max value:3000)</label>

                        <div class="container">
                            <div id="right" style="margin-right:20px;"><input type="number" min="0" class="form-control margin" placeholder="low Quantity value" name="lowquantity" value="<?php if (isset($_POST['lowquantity'])) echo $_POST['lowquantity'] ?>"></div>
                            <div>to</div>

                            <div id="left" style="margin-left:10px;"><input type="number" min="0" class="form-control margin" placeholder="high Quantity value " name="highquantity" value="<?php if (isset($_POST['highquantity'])) echo $_POST['highquantity']  ?>"></div>
                        </div>

                        <button type="submit" class="btn btn-primary btn-md" name="filterbutton">Filter</button>
                    </div>

                </form>

                <thead>
                    <tr class="bg-dark text-white text-center">


                        <th>Image</th>
                        <th>Category Name</th>
                        <th>Name</th>
                        <th>Product Code</th>
                        <?php echo "<th><a href='?order=fPrice&&sort=$sort'>Price</a></th>";
                        echo "<th><a href='?order=fSalePrice&&sort=$sort'>Sale Price</a></th>";
                        echo "<th><a href='?order=iQuantity&&sort=$sort'>Quantity</a></th>"; ?>
                        <th>Orders</th>
                        <th>Status</th>
                        <th>Added Date</th>
                        <th>Modified Date</th>
                        <th>Action</th>

                    </tr>
                </thead>
                <?php

                if (mysqli_num_rows($result) == 0) {
                    echo ("<caption class='caption'>No Records Found</caption>");
                } else {

                    while ($row = mysqli_fetch_assoc($result)) {

                        echo ("<td><img src=\"upload/product/thumbnail/" . $row['productimage'] . "\" class='imgsize'</td>");
                        echo ("<td>$row[categoryName]</td>");
                        echo ("<td>$row[vName]</td>");
                        echo ("<td>$row[vProductCode]</td>");
                        echo ("<td>$row[fPrice]");
                        echo ("<td>$row[fSalePrice]");
                        echo ("<td>$row[iQuantity]</td>");
                        echo ("<td>$row[iOrder]</td>");
                        echo ("<td>$row[enStatus]</td>");
                        echo ("<td>$row[dtAddedDate]</td>");
                        echo ("<td>$row[dtModifiedDate]</td>");
                        echo ("<td><form method='POST'><button type='submit' name='editbutton' value='$row[iProductId]' class='btn btn-outline-danger' >Edit</button></form>");
                        echo ("<button type='button' class='btn btn-outline-success'><a href=\"product_delete.php?id=" . $row['iProductId'] . "\">  Delete </a></button></td></tr>");
                    }
                }
                ?>

            </table>
        </div>
    </div>
</body>

</html>