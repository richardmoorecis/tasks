<?php
include 'connection.php';
include 'common/configuration.php';


$email = "";
$password = "";
$errors = array();
if (isset($_SESSION['username'])) {

	location("category.php");
} else {
	if (isset($_POST['submit'])) {
		$email = mysqli_real_escape_string($conn, $_POST['email']);
		$password = mysqli_real_escape_string($conn, $_POST['password']);
		$dt1 = date("Y-m-d h:i:s");

		if (empty($email)) {
			array_push($errors, "Email is required");
		}
		if (empty($password)) {
			array_push($errors, "Password is required");
		}


		if (count($errors) == 0) {

			$password = md5($password);
			$result = $randomvariable->select(
				$selectStar,
				"users ",
				"  ",
				" vEmail='$email' AND vPassword='$password'  ",
				" ",
				" "
			);

			if (mysqli_num_rows($result) == 1) {

				$result = mysqli_fetch_array($result);
				$_SESSION['username'] = $result['vUserName'];
				$_SESSION['email'] = $email;



				$update = $randomvariable->update(
					"users",
					"dtLastloginAt='$dt1'",
					" vEmail='$email'"
				);



				location("category.php");
			} else {
				array_push($errors, "Wrong email/password combination");
			}
		}
	}
}


?>
<!DOCTYPE html>
<html>

<head>
	<title>User-Login</title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
	<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
	<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.1/jquery.validate.min.js"></script>
	<link rel="stylesheet" href="style.css">
</head>

<body>
	<div id="login">
		<div>
			<div id="login-row" class="row justify-content-center align-items-center">
				<div id="login-column" class="col-md-6">
					<div id="login-box" class="col-md-12">
						<form id="form" class="form" method="POST">
							<br />
							<br />
							<?php include('errors.php'); ?>
							<h3 class="text-center text-info">Login </h3>
							<div class="form-group">
								<label for="email" class="text-info">Email:</label><br>
								<input type="email" id="email" name="email" class="form-control">
							</div>

							<div class="form-group">
								<label for="password" class="text-info">Password:</label><br>
								<input type="password" id="password" name="password" class="form-control">
							</div>


							<div class="form-group text-center">

								<input type="submit" name="submit" class="btn btn-primary btn-md"><br /><br />
							</div>
							<p>
								<a href="registration.php">Not a User! Sign up</a>
							</p>

						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		$(document).ready(function() {
			$('#form').validate({
				rules: {

					email: {
						required: true,
						email: true
					},
					password: {
						required: true,
						minlength: 8
					}

				},
				messages: {

					email: {
						required: 'Please enter Email Address.',
						email: 'Please enter a valid Email Address.',
					},

					password: {
						required: 'Please enter Password.',
						minlength: 'Password must be at least 8 characters long.',
					}

				}

			});
		});
	</script>

</body>

</html>