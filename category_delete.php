<?php
include 'connection.php';
include 'common/configuration.php';


if (!$conn) {
    echo "Error in connection" . mysqli_connect_error();
}
$id = $_GET['id'];

$result1 = $randomvariable->select(
    $selectStar,
    "categories ",
    "",
    "  iCategoryId='$id'",
    "",
    ""
);
$fetchedcategory = mysqli_fetch_assoc($result1);


categoryunlinkfunction($fetchedcategory['vImage'], $categoryimgpath, $categorythumpath);


$result = $randomvariable->select(
    $selectStar,
    "products ",
    " ",
    " iCategoryId = '$id' ",
    " ",
    " "
);

while ($singleproduct = mysqli_fetch_array($result)) {
    $productId = $singleproduct['iProductId'];


    $prodrow = $randomvariable->select(
        $selectStar,
        "product_images ",
        " ",
        " iProductId='$productId' ",
        " ",
        " "
    );

    while ($row = mysqli_fetch_assoc($prodrow)) {

        productunlinkfunction($row['vImage'], $productimgpath, $productthumpath);
    }

    $randomvariable->delete("product_images", "iProductId='$productId'");
    $randomvariable->delete("products", "iProductId='$productId'");
}

$randomvariable->delete("categories", "iCategoryId='$id'");

header('location:category.php');
