<?php
$selectStar="SELECT  * FROM "; 
$categoryimgpath="upload/category/";
$categorythumpath="upload/category/thumbnail/";
$productimgpath="upload/product/";
$productthumpath="upload/product/thumbnail/";
 //category
 $selectCategoryGetData="SELECT categories.iCategoryId, categories.vImage, categories.vName, categories.iOrder,categories.enStatus, categories.dtAddedDate, categories.dtModifiedDate,  COUNT(products.iProductId) AS productCount FROM ";
 $leftjoinCategoryGetData=" LEFT JOIN products ON categories.iCategoryId=products.iCategoryId ";
 $categorygroupby=" GROUP BY categories.vName ";

 //product
 $selectProductGetData="SELECT products.iProductId,products.vName,products.vProductCode,products.fPrice,products.fSalePrice,products.iQuantity,products.iOrder,products.enStatus,products.dtAddedDate,products.dtModifiedDate, categories.vName AS categoryName,product_images.vImage AS productimage FROM";
 $leftjoinProductGetData=" LEFT JOIN categories ON products.iCategoryId=categories.iCategoryId LEFT JOIN product_images ON products.iProductId=product_images.iProductId and product_images.enIsMainImage='Yes' ";
 $productimagesinsert="INSERT INTO `product_images`(`vImage`, `iOrder`, `iProductId`, `enIsMainImage`, `dtAddedAt`, `dtModifiedDate`) VALUES ";
